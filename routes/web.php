<?php
Use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return view('home');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');
Route::group(['middleware' => ['auth']], function () {
    Route::resource('admin/user', 'AdminUserController')->middleware('is_admin');
    Route::resource('admin/user_detail', 'AdminUserDetailController')->middleware('is_admin')->only(['index','store']);
    Route::resource('user_detail', 'UserDetailController')->only(['index','store']);
    Route::resource('admin/produk', 'AdminProdukController')->middleware('is_admin');
    Route::resource('produk', 'ProdukController')->only(['index','show']);
    Route::resource('admin/kategori', 'AdminKategoriController')->middleware('is_admin');
    Route::resource('kategori', 'KategoriController')->only(['index','show']);
    Route::resource('admin/tag', 'AdminTagController')->middleware('is_admin');
    Route::resource('tag', 'TagController')->only(['index','show']);
    Route::resource('penilaian', 'PenilaianController');
    Route::resource('admin/transaksi', 'AdminTransaksiController')->middleware('is_admin');
    Route::resource('admin/jasa_kirim', 'AdminJasaKirimController')->middleware('is_admin');
    Route::resource('jasa_kirim', 'JasaKirimController')->only(['index','show']);
    Route::resource('checkout', 'CheckoutController');
    Route::get('/download-checkout', 'CheckoutController@pdf');
    Route::resource('detail_pesanan', 'DetailPesananController');
});
Route::resource('produk', 'ProdukController');

Auth::routes();


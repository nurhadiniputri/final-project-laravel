<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPesanan extends Model
{
    protected $table = 'detail_pesanan';
    protected $fillable = [
        'user_id', 'produk_id', 'jumlah_pesanan', 'permintaan_tambahan'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Kategori;
use File;

class AdminProdukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        return view('admin.produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.produk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'stok' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
            'gambar' => 'mimes:jpeg,jpg,png|max:2200'
        ]);

        $img = $request->gambar;
        $new_img = time().'-'.$img->getClientOriginalName();


        $kategori_arr = explode(',', $request['kategori']);
        $ktg_id = [];
        foreach ($kategori_arr as  $kategori_nama) {
            $ktg = Kategori::where('nama', $kategori_nama)->first();
            if ($ktg) {
                $ktg_id[] = $ktg->id;
            } else {
                $new_ktg = Kategori::create(['nama' => $kategori_nama]);
                $ktg_id[] = $new_ktg->id;
            }  
        }

        $query = Produk::create([
            'nama' => $request["nama"],
            'stok' => $request["stok"],
            'deskripsi' => $request["deskripsi"],
            'harga' => $request["harga"],
            'gambar' => $new_img
        ]);

        $query->kategori()->sync($ktg_id);

        $img->move('uploads/produk/', $new_img);
        return redirect('admin/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = Produk::findorfail($id);
        return view('admin.produk.show', compact('produk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::findorfail($id);
        return view('admin.produk.edit',compact('produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'stok' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
            'gambar' => 'mimes:jpeg,jpg,png|max:2200'
        ]);

        $kategori_arr = explode(',', $request['kategori']);
        $ktg_id = [];
        foreach ($kategori_arr as  $kategori_nama) {
            $ktg = Kategori::where('nama', $kategori_nama)->first();
            if ($ktg) {
                $ktg_id[] = $ktg->id;
            } else {
                $new_ktg = Kategori::create(['nama' => $kategori_nama]);
                $ktg_id[] = $new_ktg->id;
            }  
        }

        $produk= Produk::findorfail($id);
        if ($request->has('gambar')) {
            $path = 'uploads/produk/';
            File::delete($path . $produk->gambar);
            $img = $request->gambar;
            $new_img = time().'-'.$img->getClientOriginalName();
            $img->move('uploads/produk/', $new_img);
            $produk_data = [
                'nama' => $request->nama,
                'stok' => $request->stok,
                'deskripsi' => $request->deskripsi,
                'harga' => $request->harga,
                'gambar' => $new_img
            ];
        }else{
            $produk_data = [
                'nama' => $request->nama,
                'stok' => $request->stok,
                'deskripsi' => $request->deskripsi,
                'harga' => $request->harga
            ];
        }
        $produk->kategori()->sync($ktg_id);
        
        $produk->update($produk_data);
        return redirect('admin/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::findorfail($id);
        $produk->delete();

        $path = 'uploads/produk/';
        File::delete($path . $produk->gambar); 
        return redirect('admin/produk');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penilaian;
use App\User;
use App\Produk;
use Auth;

class PenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penilaian = Penilaian::where('user_id', Auth::user()->id)->get();
        return view('penilaian.index', compact('penilaian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Penilaian::where('user_id', Auth::user()->id)->get();
        $produk = Produk::all();
        return view('penilaian.create',compact('produk','user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'produk_id' => 'required',
            'point' => 'required',
            'komentar' => ''
        ]);
        $query = Penilaian::create([
            'user_id' => Auth::user()->id,
            'produk_id' => $request["produk_id"],
            'point' => $request["point"],
            'komentar' => $request["komentar"]
        ]);
        return redirect('/penilaian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penilaian = Penilaian::findorfail($id);
        return view('penilaian.show',compact('penilaian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Penilaian::where('user_id', Auth::user()->id)->first();
        $produk = Produk::all();
        $penilaian = Penilaian::findorfail($id);
        return view('penilaian.edit',compact('penilaian','produk','user_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'produk_id' => 'required',
            'point' => 'required',
            'komentar' => ''
        ]);
        $penilaian = Penilaian::findorfail($id);
        $penilaian_data = [
            'user_id' => Auth::User()->id,
            'produk_id' => $request->produk_id,
            'point' => $request->point,
            'komentar' => $request->komentar,
        ];
        $penilaian->update($penilaian_data);
        return redirect('/penilaian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penilaian = Penilaian::findorfail($id);
        $penilaian->delete();
        return redirect('/penilaian');
    }
}

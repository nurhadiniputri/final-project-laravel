<?php

namespace App\Http\Controllers;

use App\JasaKirim;
use Illuminate\Http\Request;

class AdminJasaKirimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jasa_kirim = JasaKirim::all();
        return view('admin.jasa_kirim.index', compact('jasa_kirim'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jasa_kirim.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'provinsi' => 'required',
            'kota_kabupaten' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'tarif' => 'required'
        ]);
        $query = JasaKirim::create([
            'nama' => $request["nama"],
            'provinsi' => $request["provinsi"],
            'kota_kabupaten' => $request["kota_kabupaten"],
            'kecamatan' => $request["kecamatan"],
            'kelurahan' => $request["kelurahan"],
            'tarif' => $request["tarif"]
        ]);
        return redirect('admin/jasa_kirim');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jasa_kirim = JasaKirim::findorfail($id);
        return view('admin.jasa_kirim.show', compact('jasa_kirim'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jasa_kirim = JasaKirim::findorfail($id);
        return view('admin.jasa_kirim.edit',compact('jasa_kirim'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'provinsi' => 'required',
            'kota_kabupaten' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'tarif' => 'required'
        ]);
        $jasa_kirim= JasaKirim::findorfail($id);
        $jasa_kirim_data = [
            'nama' => $request->nama,
            'provinsi' => $request->provinsi,
            'kota_kabupaten' => $request->kota_kabupaten,
            'kecamatan' => $request->kecamatan,
            'kelurahan' => $request->kelurahan,
            'tarif' => $request->tarif
        ];
        $jasa_kirim->update($jasa_kirim_data);
        return redirect('admin/jasa_kirim');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jasa_kirim = JasaKirim::findorfail($id);
        $jasa_kirim->delete();
        return redirect('admin/jasa_kirim');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Checkout;
use App\User;
use App\Transaksi;
use App\JasaKirim;
use Auth;
use PDF;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $checkout = Checkout::where('user_id', Auth::user()->id)->get();
        return view('checkout.index', compact('checkout'));
    }

    //DOM PDF
    public function pdf(){
        $checkout = Checkout::where('user_id', Auth::user()->id)->get();
        $pdf = PDF::loadView('pdf.checkout', compact('checkout'));
        return $pdf->download('checkout.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Checkout::where('user_id', Auth::user()->id)->first();
        $transaksi = Transaksi::all();
        $jasa_kirim = JasaKirim::all();
        return view('checkout.create',compact('transaksi','jasa_kirim','user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jasa_kirim_id' => 'required',
            'alamat' => 'required',
            'kodepos' => 'required',
            'no_telp' => 'required',
            'tgl_pesan' => 'required',
            'total' => 'required',
            'transaksi_id' => 'required',
            'tgl_pembayaran' => ''
        ]);
        $query = Checkout::create([
            'user_id' => Auth::User()->id,
            'jasa_kirim_id' => $request["jasa_kirim_id"],
            'alamat' => $request["alamat"],
            'kodepos' => $request["kodepos"],
            'no_telp' => $request["no_telp"],
            'tgl_pesan' => $request["tgl_pesan"],
            'total' => $request["total"],
            'transaksi_id' => $request["transaksi_id"],
            'tgl_pembayaran' => $request["tgl_pembayaran"]
        ]);
        return redirect('/checkout');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $checkout = Checkout::findorfail($id);
        return view('checkout.show',compact('checkout'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Checkout::where('user_id', Auth::user()->id)->first();
        $transaksi = Transaksi::all();
        $jasa_kirim = JasaKirim::all();
        $checkout = Checkout::findorfail($id);
        return view('checkout.edit',compact('checkout','user_id','transaksi','jasa_kirim'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'jasa_kirim_id' => 'required',
            'alamat' => 'required',
            'kodepos' => 'required',
            'no_telp' => 'required',
            'tgl_pesan' => 'required',
            'total' => 'required',
            'transaksi_id' => 'required',
            'tgl_pembayaran' => ''
        ]);
        $checkout = Checkout::findorfail($id);
        $checkout_data = [
            'user_id' => Auth::User()->id,
            'jasa_kirim_id' => $request->jasa_kirim_id,
            'alamat' => $request->alamat,
            'kodepos' => $request->kodepos,
            'no_telp' => $request->no_telp,
            'tgl_pesan' => $request->tgl_pesan,
            'total' => $request["total"],
            'transaksi_id' => $request->transaksi_id,
            'tgl_pembayaran' => $request->tgl_pembayaran
        ];
        $checkout->update($checkout_data);
        return redirect('/checkout');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkout = Checkout::findorfail($id);
        $checkout->delete();
        return redirect('/checkout');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailPesanan;
use App\User;
use App\Produk;
use Auth;

class DetailPesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detail_pesanan = DetailPesanan::where('user_id', Auth::user()->id)->get();
        return view('detail_pesanan.index', compact('detail_pesanan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = DetailPesanan::where('user_id', Auth::user()->id)->get();
        $produk = Produk::all();
        return view('detail_pesanan.create',compact('produk','user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'produk_id' => 'required',
            'jumlah_pesanan' => 'required',
            'permintaan_tambahan' => ''
        ]);
        $query = DetailPesanan::create([
            'user_id' => Auth::user()->id,
            'produk_id' => $request["produk_id"],
            'jumlah_pesanan' => $request["jumlah_pesanan"],
            'permintaan_tambahan' => $request["permintaan_tambahan"]
        ]);
        return redirect('/detail_pesanan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail_pesanan = DetailPesanan::findorfail($id);
        return view('detail_pesanan.show',compact('detail_pesanan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = DetailPesanan::where('user_id', Auth::user()->id)->first();
        $produk = Produk::all();
        $detail_pesanan = DetailPesanan::findorfail($id);
        return view('detail_pesanan.edit',compact('detail_pesanan','produk','user_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'produk_id' => 'required',
            'jumlah_pesanan' => 'required',
            'permintaan_tambahan' => ''
        ]);

        $detail_pesanan = DetailPesanan::findorfail($id);
        $detail_pesanan_data = [
            'user_id' => Auth::user()->id,
            'produk_id' => $request->produk_id,
            'jumlah_pesanan' => $request->jumlah_pesanan,
            'permintaan_tambahan' => $request->permintaan_tambahan,
        ];
        $detail_pesanan->update($detail_pesanan_data);
        return redirect('/detail_pesanan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail_pesanan = DetailPesanan::findorfail($id);
        $detail_pesanan->delete();
        return redirect('/detail_pesanan');
    }
}

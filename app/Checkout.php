<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $table = 'checkout';
    protected $fillable = [
        'user_id', 'jasa_kirim_id', 'alamat', 'kodepos', 'no_telp', 'tgl_pesan', 'total', 'transaksi_id', 'tgl_pembayaran'
    ];
    public function transaksi()
    {
        return $this->belongsTo('App\Transaksi');
    }
    public function jasa_kirim()
    {
        return $this->belongsTo('App\JasaKirim');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

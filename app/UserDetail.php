<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = 'user_detail';
    protected $fillable = [
        'tgl_lahir', 'bio', 'no_hp', 'user_id'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

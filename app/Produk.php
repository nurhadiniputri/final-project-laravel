<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $fillable = [
        'nama', 'stok', 'deskripsi', 'harga', 'gambar'
    ];
    public function detail_pesanan()
    {
        return $this->hasMany('App\DetailPesanan');
    }
    public function tag()
    {
        return $this->hasMany('App\Tag');
    }
    public function penilaian()
    {
        return $this->hasMany('App\Penilaian');
    }
    public function kategori()
    {
        return $this->belongsToMany('App\Kategori', 'tag','produk_id','kategori_id');
    }
}

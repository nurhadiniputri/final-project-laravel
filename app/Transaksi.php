<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $fillable = [
        'metode'
    ];
    public function checkout()
    {
        return $this->hasMany('App\Checkout');
    }
}

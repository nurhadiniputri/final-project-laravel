<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JasaKirim extends Model
{
    protected $table = 'jasa_kirim';
    protected $fillable = [
        'nama', 'provinsi', 'kota_kabupaten', 'kecamatan', 'kelurahan', 'tarif'
    ];
    public function checkout()
    {
        return $this->hasMany('App\Checkout');
    }
}

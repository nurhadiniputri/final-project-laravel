<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tag';
    protected $fillable = [
        'produk_id', 'kategori_id'
    ];
    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }
}

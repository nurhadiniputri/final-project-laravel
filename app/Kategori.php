<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = [
        'nama'
    ];
    protected $guarded = [];
    public function tag()
    {
        return $this->hasMany('App\Tag');
    }
}

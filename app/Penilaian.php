<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    protected $table = 'penilaian';
    protected $fillable = [
        'user_id', 'produk_id', 'point', 'komentar'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
}

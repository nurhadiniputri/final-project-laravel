@extends('layouts.shop')
@section('judul')
    <b>Detail Penilaian</b>
@endsection
@section('isi')
    <h4>Detail Penilaian dengan Id {{$penilaian->id}}</h4><br>
    <p>User : {{$penilaian->user->name}}</p>
    <p>Produk : {{$penilaian->produk->nama}}</p>
    <p>Gambar : 
        <img class="card-img-top" style="width: 500px;" src="{{asset('uploads/produk/'.$penilaian->produk->gambar)}}" alt="Card image cap">
    </p>
    <p>Point : {{$penilaian->point}}/5</p>
    <p>Komentar : 
        @if($penilaian->komentar != null)
            {{$penilaian->komentar}}
        @else
          No Comment
        @endif
    </p>
    <a href="/penilaian/" class="btn btn-info">Back</a>
@endsection
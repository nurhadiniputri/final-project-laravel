@extends('layouts.shop')
@section('judul')
    <b>List Penilaian</b>
@endsection
@section('isi')
<a href="/penilaian/create" class="btn btn-primary mb-2">Tambah</a>
<div class="table-responsive">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Produk</th>
      <th scope="col">User</th>
      <th scope="col">Point</th>
      <th scope="col">Komentar</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($penilaian as $key => $item)  
    <tr>
      <td>{{$key+1}}</td>
      <td>{{$item->user->name}}</td>
      <td>{{$item->produk->nama}}</td>
      <td>{{$item->point}}/5</td>
      <td>
        @if($item->komentar != null)
          {{$item->komentar}}
        @else
          <p>No comment</p>
        @endif
      </td>
      <td>
        <form action="/penilaian/{{$item->id}}" method="POST">
        <a href="/penilaian/{{$item->id}}" class="btn btn-info">Detail</a>
        <a href="/penilaian/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
@extends('layouts.shop')
@section('judul')
    <b>Tambah Data Penilaian</b>
@endsection
@section('isi')
<div>
        <form action="/penilaian" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="form-group">
                <label>Produk</label>
                <select class="form-control" name="produk_id">
                    <option>Pilih Produk</option>
                    @foreach ($produk as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
                @error('produk_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Point</label>
                <input type="number" class="form-control" name="point" placeholder="Nilai kami dengan skala 5">
                @error('point')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Komentar</label>
                <textarea class="form-control" name="komentar" placeholder="Feedback"></textarea>
                @error('komentar')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/penilaian/" class="btn btn-info">Back</a>
        </form>
</div>
@endsection
@extends('layouts.shop')
@section('judul')
    <b>Edit Penilaian</b>
@endsection
@section('isi')
<div>
    <form action="/penilaian/{{$penilaian->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Produk</label>
            <select class="form-control" name="produk_id">
                <option>Pilih Produk</option>
                @foreach ($produk as $item)
                @if ($item->id === $item->produk_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
                @endforeach
            </select>
            @error('produk_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Point</label>
            <input type="number" class="form-control" name="point" value="{{$penilaian->point}}" placeholder="Masukkan point">
            @error('point')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Komentar</label>
            <textarea class="form-control" name="komentar" placeholder="Masukkan komentar">{{$penilaian->komentar}}</textarea>
            @error('komentar')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>    
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/penilaian/" class="btn btn-info">Back</a>
    </form>
</div>
@endsection
@extends('layouts.shop')
@section('judul')
    <b>Tambah Data Detail Pesanan</b>
@endsection
@section('isi')
<div>
        <form action="/detail_pesanan" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="form-group">
                <label>Produk</label>
                <select class="form-control" name="produk_id">
                    <option>Pilih Produk</option>
                    @foreach ($produk as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
                @error('produk_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Jumlah Pesanan</label>
                <input type="number" class="form-control" name="jumlah_pesanan" placeholder="Masukkan jumlah pesanan">
                @error('jumlah_pesanan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Permintaan Tambahan</label>
                <textarea class="form-control" name="permintaan_tambahan" placeholder="Masukkan permintaan tambahan"></textarea>
                @error('permintaan_tambahan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/detail_pesanan/" class="btn btn-info">Back</a>
        </form>
</div>
@endsection
@extends('layouts.shop')
@section('judul')
    <b>Edit Detail Pesanan</b>
@endsection
@section('isi')
<div>
    <form action="/detail_pesanan/{{$detail_pesanan->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Produk</label>
            <select class="form-control" name="produk_id">
                <option>Pilih Produk</option>
                @foreach ($produk as $item)
                @if ($item->id === $item->produk_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
                @endforeach
            </select>
            @error('produk_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Jumlah Pesanan</label>
            <input type="number" class="form-control" name="jumlah_pesanan" value="{{$detail_pesanan->jumlah_pesanan}}" placeholder="Masukkan jumlah pesanan">
            @error('jumlah_pesanan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Permintaan Tambahan</label>
            <textarea class="form-control" name="permintaan_tambahan" placeholder="Masukkan permintaan tambahan">{{$detail_pesanan->permintaan_tambahan}}</textarea>
            @error('permintaan_tambahan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/detail_pesanan/" class="btn btn-info">Back</a>
    </form>
</div>
@endsection
@extends('layouts.shop')
@section('judul')
    <b>Detail Pesanan</b>
@endsection
@section('isi')
    <p>Cart : {{$detail_pesanan->user->name}}</p>
    <p>Produk : {{$detail_pesanan->produk->nama}}</p>
    <p>Gambar : 
        <img class="card-img-top" style="width: 500px;" src="{{asset('uploads/produk/'.$detail_pesanan->produk->gambar)}}" alt="Card image cap">
    </p>
    <p>Jumlah Pesanan : {{$detail_pesanan->jumlah_pesanan}}</p>
    <p>Permintaan Tambahan : 
        @if($detail_pesanan->permintaan_tambahan != null)
            {{$detail_pesanan->permintaan_tambahan}}
        @else
          Tidak ada
        @endif
    </p>
    <a href="/detail_pesanan/" class="btn btn-info">Back</a>
@endsection
@extends('layouts.shop')
@section('judul')
    <b>List Detail Pesanan dalam Cart</b>
@endsection
@section('isi')
<a href="/detail_pesanan/create" class="btn btn-primary mb-2">Tambah</a>
<div class="table-responsive">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Cart</th>
      <th scope="col">Gambar</th>
      <th scope="col">Produk</th>
      <th scope="col">Jumlah Pesanan</th>
      <th scope="col">Permintaan Tambahan</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($detail_pesanan as $key => $item)  
    <tr>
      <td>{{$key+1}}</td>
      <td>{{$item->user->name}}</td>
      <td><img class="card-img-top" style="width: 10rem;" src="{{asset('uploads/produk/'.$item->produk->gambar)}}" alt="Card image cap"></td>
      <td>{{$item->produk->nama}}</td>
      <td>{{$item->jumlah_pesanan}}</td>
      <td>
        @if($item->permintaan_tambahan != null)
          {{$item->permintaan_tambahan}}
        @else
          <p>Tidak ada</p>
        @endif
      </td>
      <td>
        <form action="/detail_pesanan/{{$item->id}}" method="POST">
        <a href="/detail_pesanan/{{$item->id}}" class="btn btn-info">Detail</a>
        <a href="/detail_pesanan/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
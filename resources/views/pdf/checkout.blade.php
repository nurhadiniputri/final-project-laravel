<style>
    table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
    }
</style><div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Cart</th>
          <th scope="col">Jasa Kirim</th>
          <th scope="col">Alamat</th>
          <th scope="col">Kodepos</th>
          <th scope="col">No Telp</th>
          <th scope="col">Tanggal Pesan</th>
          <th scope="col">Total Belanja</th>
          <th scope="col">Metode Pembayaran</th>
          <th scope="col">Tanggal Pembayaran</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($checkout as $key => $item)  
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$item->user->name}}</td>
          <td>{{$item->jasa_kirim->nama}}</td>
          <td>{{$item->alamat}}</td>
          <td>{{$item->kodepos}}</td>
          <td>{{$item->no_telp}}</td>
          <td>{{$item->tgl_pesan}}</td>
          <td>{{$item->total}}</td>
          <td>{{$item->transaksi->metode}}</td>
          <td>{{$item->tgl_pesan}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    </div>
@extends('layouts.shop')
@section('judul')
    <b>Detail Produk</b>
@endsection
@section('isi')
    <style>
        p{
            padding-left: 15px;
        }
    </style>
    <h4><b>{{$produk->nama}}</b></h4>
    <p>Posted {{ $produk->created_at->diffForHumans() }}</p>
    <img src="{{asset('uploads/produk/'.$produk->gambar)}}" style="height: 400px" alt="...">
    <p>Stok: {{$produk->stok}}</p>
    <p>{{$produk->deskripsi}}</p>
    <p>Rp{{$produk->harga}}</p><br>

    @foreach ($produk->kategori as $value)
    <a href="/kategori/{{$value->id}}" class="badge badge-success">{{$value->nama}}</a>
    @endforeach
    <div style="border: 2px outset black;"">
        @foreach ($produk->penilaian as $key => $value)
        <p><b>Komentar {{$key+1}}</b> -> posted {{ $value->created_at->diffForHumans() }}</p>
        <p>User : {{$value->user->email}}</p>
        <p>Point : {{$value->point}}</p>
        <p>Komentar : {{$value->komentar}}</p>
        @endforeach
    </div>
    <br>
    <a href="/produk/" class="btn btn-info mt-2">Back</a>
@endsection
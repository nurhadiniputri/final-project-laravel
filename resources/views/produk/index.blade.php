@extends('layouts.shop')
@section('judul')
    <b>List Produk</b>
@endsection
@section('isi')
<div class="row">
    @foreach ($produk as $item)  
    <div class="col-4">
      <div class="card mt-2" style="width: 18rem;">
        <img class="card-img-top" style="height: 200px" src="{{asset('uploads/produk/'.$item->gambar)}}" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">{{$item->nama}}</h5>
          <p class="card-text">Stok: {{$item->stok}}</p>
          <p class="card-text">{{Str::limit($item->deskripsi,100)}}</p>
          <p class="card-text">Rp{{$item->harga}}</p>
          @foreach ($item->kategori as $value)
          <a href="/kategori/{{$value->id}}" class="badge badge-success">{{$value->nama}}</a>
          @endforeach
          @guest
          @else
          <br>
            <a href="/produk/{{$item->id}}" class="btn btn-info mt-1">Detail</a>
          @endguest
        </div>
      </div>
    </div>
    @endforeach
</div>
@endsection
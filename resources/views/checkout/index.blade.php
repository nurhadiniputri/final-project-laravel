@extends('layouts.shop')
@section('judul')
    <b>List Checkout</b>
@endsection
@section('isi')
<a href="/checkout/create" class="btn btn-primary mb-2">Tambah</a>
<a href="/download-checkout" class="btn btn-info mb-2">Download PDF</a>
<div class="table-responsive">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Cart</th>
      <th scope="col">Jasa Kirim</th>
      <th scope="col">Alamat</th>
      <th scope="col">Kodepos</th>
      <th scope="col">No Telp</th>
      <th scope="col">Tanggal Pesan</th>
      <th scope="col">Total Belanja</th>
      <th scope="col">Metode Pembayaran</th>
      <th scope="col">Tanggal Pembayaran</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($checkout as $key => $item)  
    <tr>
      <td>{{$key+1}}</td>
      <td>{{$item->user->name}}</td>
      <td>{{$item->jasa_kirim->nama}}</td>
      <td>{{$item->alamat}}</td>
      <td>{{$item->kodepos}}</td>
      <td>{{$item->no_telp}}</td>
      <td>{{$item->tgl_pesan}}</td>
      <td>{{$item->total}}</td>
      <td>{{$item->transaksi->metode}}</td>
      <td>{{$item->tgl_pembayaran}}</td>
      <td>
        <form action="/checkout/{{$item->id}}" method="POST">
        <a href="/checkout/{{$item->id}}" class="btn btn-info">Detail</a>
        <a href="/checkout/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
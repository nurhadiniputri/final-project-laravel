@extends('layouts.shop')
@section('judul')
    <b>Detail Checkout</b>
@endsection
@section('isi')
    <p>Cart milik {{$checkout->cart->user->name}}</p>
    <p>Metode Transaksi : {{$checkout->transaksi->metode}}</p>
    <p>Jasa Kirim : {{$checkout->jasa_kirim->nama}}</p>
    <p>Alamat : {{$checkout->alamat}}</p>
    <p>Kodepos : {{$checkout->kodepos}}</p>
    <p>No Telp : {{$checkout->no_telp}}</p>
    <p>Total pembayaran : {{$checkout->total}}</p>
    <p>Tanggal Pesan : {{$checkout->tgl_pesan}}</p>
    <a href="/checkout/" class="btn btn-info">Back</a>
@endsection
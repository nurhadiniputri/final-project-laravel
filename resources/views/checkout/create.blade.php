@extends('layouts.shop')
@section('judul')
    <b>Tambah Data Checkout</b>
@endsection
@section('isi')
<div>
    <b>Catatan :</b><br>
    <p>Sebelum melakukan checkout, harap cek <a href="/detail_pesanan">cart</a>.</p>
    <p>Karena semua yang ada dalam cart otomatis <b>tercheckout</b>.</p>
        <form action="/checkout" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="form-group">
                <label>Jasa Kirim</label>
                <select class="form-control" name="jasa_kirim_id">
                    <option>Pilih jasa kirim</option>
                    @foreach ($jasa_kirim as $item)
                        <option value="{{$item->id}}">{{$item->nama}} untuk wilayah {{$item->provinsi}}, {{$item->kota_kabupaten}}, {{$item->kecamatan}}, {{$item->kelurahan}}, dengan tarif {{$item->tarif}}</option>
                    @endforeach
                </select>
                @error('jasa_kirim_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <textarea class="form-control" name="alamat" placeholder="Masukkan alamat"></textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Kodepos</label>
                <input type="number" class="form-control" name="kodepos" placeholder="Masukkan kodepos">
                @error('kodepos')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>No Telepon</label>
                <input type="text" class="form-control" name="no_telp" placeholder="Masukkan no telp">
                @error('no_telp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Tanggal Pesan</label>
                <input type="date" class="form-control" name="tgl_pesan" placeholder="Masukkan tanggal pesan">
                @error('tgl_pesan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Total Pembayaran</label>
                <input type="number" class="form-control" name="total" placeholder="Masukkan total">
                @error('total')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Transaksi</label>
                <select class="form-control" name="transaksi_id">
                    <option>Pilih transaksi</option>
                    @foreach ($transaksi as $item)
                        <option value="{{$item->id}}">{{$item->metode}}</option>
                    @endforeach
                </select>
                @error('transaksi_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Tanggal Pembayaran</label>
                <input type="date" class="form-control" name="tgl_pembayaran" placeholder="Masukkan tanggal pembayaran">
                @error('tgl_pembayaran')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/checkout/" class="btn btn-info">Back</a>
        </form>
</div>
@endsection
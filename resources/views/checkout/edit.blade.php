@extends('layouts.shop')
@section('judul')
    <b>Edit Checkout</b>
@endsection
@section('isi')
<div>
    <form action="/checkout/{{$checkout->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Jasa Kirim</label>
            <select class="form-control" name="jasa_kirim_id">
                <option>Pilih jasa kirim</option>
                @foreach ($jasa_kirim as $item)
                @if ($item->id === $item->jasa_kirim_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}} untuk wilayah {{$item->provinsi}}, {{$item->kota_kabupaten}}, {{$item->kecamatan}}, {{$item->kelurahan}}, dengan tarif {{$item->tarif}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}} untuk wilayah {{$item->provinsi}}, {{$item->kota_kabupaten}}, {{$item->kecamatan}}, {{$item->kelurahan}}, dengan tarif {{$item->tarif}}</option>
                @endif
                @endforeach
            </select>
            @error('jasa_kirim_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Alamat</label>
            <textarea class="form-control" name="alamat" placeholder="Masukkan alamat">{{checkout->alamat}}</textarea>
            @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Kodepos</label>
            <input type="number" class="form-control" name="kodepos" value="{{checkout->kodepos}}" placeholder="Masukkan kodepos">
            @error('kodepos')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>No Telepon</label>
            <input type="text" class="form-control" name="no_telp" value="{{checkout->no_telp}}" placeholder="Masukkan no telp">
            @error('no_telp')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Tanggal Pesan</label>
            <input type="date" class="form-control" name="tgl_pesan" value="{{checkout->tgl_pesan}}" placeholder="Masukkan tanggal pesan">
            @error('tgl_pesan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Total Pembayaran</label>
            <input type="number" class="form-control" name="total" value="{{checkout->total}}" placeholder="Masukkan total">
            @error('total')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Transaksi</label>
            <select class="form-control" name="transaksi_id">
                <option>Pilih transaksi</option>
                @foreach ($transaksi as $item)
                @if ($item->id === $item->transaksi_id)
                    <option value="{{$item->id}}" selected>{{$item->metode}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->metode}}</option>
                @endif
                @endforeach
            </select>
            @error('transaksi_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Tanggal pembayaran</label>
            <input type="date" class="form-control" name="tgl_pembayaran" value="{{checkout->tgl_pembayaran}}" placeholder="Masukkan tanggal pembayaran">
            @error('tgl_pembayaran')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/checkout/" class="btn btn-info">Back</a>
    </form>
</div>
@endsection
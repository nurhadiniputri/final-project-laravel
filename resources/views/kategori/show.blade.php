@extends('layouts.shop')
@section('judul')
    <b>Detail Kategori</b>
@endsection
@section('isi')
    <p>Nama : {{$kategori->nama}}</p>
    <p>List Barang:</p>
    <ul>
        @foreach ($kategori->tag as $value)
          <li>{{$value->produk->nama}}</li>
        @endforeach
      </ul>
    <a href="/produk/" class="btn btn-info">Back</a>
@endsection
@extends('layouts.shop')
@section('judul')
    <b>List Kategori</b>
@endsection
@section('isi')
<div class="table-responsive">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Kategori</th>
      <th scope="col">Nama Produk</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($kategori as $key => $item)  
    <tr>
      <td>{{$key+1}}</td>
      <td>{{$item->nama}}</td>
      <td>
        <div class="d-flex justify-content-center">
          <ul>
            @foreach ($item->tag as $value)
              <li>{{$value->produk->nama}}</li>
            @endforeach
          </ul>
        </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
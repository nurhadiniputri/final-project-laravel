@extends('admin.layouts.shop')
@section('judul')
    <b>Detail Transaksi</b>
@endsection
@section('isi')
    <p>Metode Pembayaran : {{$transaksi->metode}}</p><br>
    <p>Pastikan pembeli membayar dengan metode yang telah dipilih.</p>
    <a href="/admin/transaksi/" class="btn btn-info">Back</a>
@endsection
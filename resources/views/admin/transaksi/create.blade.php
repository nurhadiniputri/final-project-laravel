@extends('admin.layouts.shop')
@section('judul')
    <b>Tambah Data Transaksi</b>
@endsection
@section('isi')
<div>
        <form action="/admin/transaksi" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="form-group">
                <label>Metode Pembayaran</label>
                <input type="text" class="form-control" name="metode" placeholder="Masukkan metode">
                @error('metode')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/admin/transaksi/" class="btn btn-info">Back</a>
        </form>
</div>
@endsection
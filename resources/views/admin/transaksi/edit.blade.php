@extends('admin.layouts.shop')
@section('judul')
    <b>Edit Transaksi</b>
@endsection
@section('isi')
<div>
    <form action="/admin/transaksi/{{$transaksi->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Metode Pembayaran</label>
            <input type="text" class="form-control" name="metode" value="{{$transaksi->metode}}" placeholder="Masukkan metode">
            @error('metode')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/admin/transaksi/" class="btn btn-info">Back</a>
    </form>
</div>
@endsection
@extends('admin.layouts.shop')
@section('judul')
    <b>List Transaksi</b>
@endsection
@section('isi')
<a href="/admin/transaksi/create" class="btn btn-primary mb-2">Tambah</a>
<div class="table-responsive">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Metode Pembayaran</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($transaksi as $key => $item)  
    <tr>
      <td>{{$key+1}}</td>
      <td>{{$item->metode}}</td>
      <td>
        <form action="/admin/transaksi/{{$item->id}}" method="POST">
        <a href="/admin/transaksi/{{$item->id}}" class="btn btn-info">Detail</a>
        <a href="/admin/transaksi/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
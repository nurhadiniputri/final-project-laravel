<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Vegefoods | E-Commerce</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('vegefoods/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vegefoods/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('vegefoods/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vegefoods/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vegefoods/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('vegefoods/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('vegefoods/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('vegefoods/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('vegefoods/css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{asset('vegefoods/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('vegefoods/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('vegefoods/css/style.css')}}">
  </head>
  @include('admin.layouts.partials.header')
    <!-- END nav -->

	<style>
		body, html {
			height: 100%;
			margin: 0;
		}
		.hero-image {
			background-image: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0)), url("/vegefoods/images/bg_1.jpg");

			height: 50%;
			opacity: 0.6;

			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			position: relative;
	}
	</style>
	<div class="hero-image">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="bread" style="color:white; padding-top: 145px;">@yield('judul')</h1>
          </div>
        </div>
      </div>
    </div>
	<section class="content">
	<div class="card-body">
		@yield('isi')
	</div>
	</section>

    <footer class="ftco-footer ftco-section">
      <div class="container">
      	<div class="row">
      		<div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
						</a>
					</div>
      	</div>
        <div class="row mb-5 d-flex justify-content-around">
          <div class="col-md d-flex justify-content-start">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Vegefoods</h2>
              <p>Mendapatkan sayur dan buah yang bagus dan berkualitas hanya pada toko kami.</p>
			  <p>Vegefoods juga menyediakan berbagai kategori sayur dan buah dengan harga yang terjangkau.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md d-flex justify-content-end">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Anda memiliki Pertanyaan?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">Jakarta, Indonesia</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+62 813 5523 4578</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">vegefoods@jakarta.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="{{asset('vegefoods/js/jquery.min.js')}}"></script>
  <script src="{{asset('vegefoods/js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{asset('vegefoods/js/popper.min.js')}}"></script>
  <script src="{{asset('vegefoods/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('vegefoods/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('vegefoods/js/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('vegefoods/js/jquery.stellar.min.js')}}"></script>
  <script src="{{asset('vegefoods/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('vegefoods/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('vegefoods/js/aos.js')}}"></script>
  <script src="{{asset('vegefoods/js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{asset('vegefoods/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('vegefoods/js/scrollax.min.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{asset('vegefoods/js/google-map.js')}}"></script>
  <script src="{{asset('vegefoods/js/main.js')}}"></script>
    
  </body>
</html>
<body class="goto-here">
    <div class="py-1 bg-primary">
    <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
            <div class="col-lg-12 d-block">
                <div class="row d-flex">
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"></div>
                        <span class="text">
                        @guest
                            No Account
                        @else
                            {{Auth::user()->email}}
                        @endguest
                        </span>
                        </div>
                    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
                        <span class="text">
                        @guest
                            Anda belum login.
                        @else
                            You are logged in!
                        @endguest
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="index.html">Vegefoods</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      @guest
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="/" class="nav-link">Laman Utama</a></li>
            <li class="nav-item active dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Produk</a>
            <div class="dropdown-menu" aria-labelledby="dropdown04">
                <a class="dropdown-item" href="/produk">List Produk</a>
            </div>
            </li>
            <li class="nav-item"><a href="/login" class="nav-link">Login</a></li>
            <li class="nav-item cta cta-colored"><a href="/detail_pesanan" class="nav-link"><span class="icon-shopping_cart"></span></a></li>
            </ul>
        </div>
      @else
      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="/admin/home" class="nav-link">Laman Utama Admin</a></li>
            <li class="nav-item active dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">User</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="/admin/user_detail">Profil Anda</a>
                        <a class="dropdown-item" href="/admin/user">List User</a>
                    </div>
                </li>
            <li class="nav-item active dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Produk</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                    <a class="dropdown-item" href="/admin/produk">List Produk</a>
                    <a class="dropdown-item" href="/admin/kategori">List Kategori</a>
                </div>
            </li>
            <li class="nav-item active dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fasilitas Pembelian</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="/admin/transaksi">Metode Transaksi</a>
                        <a class="dropdown-item" href="/admin/jasa_kirim">Jasa Kirim</a>
                    </div>
                </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            </ul>
        </div>
        @endguest
    </div>
  </nav>
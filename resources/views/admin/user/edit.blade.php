@extends('admin.layouts.shop')
@section('judul')
    <b>Edit User</b>
@endsection
@section('isi')
<div>
    <form action="/admin/user/{{$user->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="name" value="{{$user->name}}" placeholder="Masukkan nama">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" value="{{$user->email}}" placeholder="Masukkan email">
            @error('email')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="text" class="form-control" name="password" value="{{$user->password}}" placeholder="Masukkan password">
            @error('password')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/admin/user/" class="btn btn-info">Back</a>
    </form>
</div>
@endsection
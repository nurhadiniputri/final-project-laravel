@extends('admin.layouts.shop')
@section('judul')
    <b>Detail User</b>
@endsection
@section('isi')
    <p>Nama : {{$user->name}}</p>
    <p>Email : {{$user->email}}</p>
    <p>Password : {{$user->password}}</p>
    @if ($user->user_detail != null)
        <p>No HP : {{$user->user_detail->no_hp}}</p>
        <p>Bio : {{$user->user_detail->bio}}</p>
        <p>Tanggal Lahir : {{$user->user_detail->tgl_lahir}}</p>
    @else
        <p>User belum menambah profil</p>
    @endif
    <p>Joined at : {{ $user->created_at->diffForHumans() }}</p>
    <a href="/admin/user/" class="btn btn-info">Back</a>
@endsection
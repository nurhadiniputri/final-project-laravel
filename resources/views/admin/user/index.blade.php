@extends('admin.layouts.shop')
@section('judul')
    <b>List User</b>
@endsection
@section('isi')
<div class="table-responsive">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Email</th>
      <th scope="col">Password</th>
      <th scope="col">Tanggal Join</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($user as $key=>$item)  
    <tr>
      <td>{{$key+1}}</td>
      <td>{{$item->name}}</td>
      <td>{{$item->email}}</td>
      <td>{{$item->password}}</td>
      <td>{{ $item->created_at->toFormattedDateString() }}</td>
      <td>
        <form action="/admin/user/{{$item->id}}" method="POST">
        <a href="/admin/user/{{$item->id}}" class="btn btn-info">Detail</a>
        <a href="/admin/user/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
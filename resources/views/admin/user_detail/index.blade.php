@extends('admin.layouts.shop')
@section('judul')
    <b>Data User</b>
@endsection
@section('isi')
@if ($user_detail != null)
<p>Nama : {{$user_detail->user->name}}</p>
<p>Email : {{$user_detail->user->email}}</p>
<div>
    <form action="/admin/user_detail/{{$user_detail->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Tanggal Lahir</label>
            <input type="date" class="form-control" name="tgl_lahir" value="{{$user_detail->tgl_lahir}}" placeholder="Masukkan tanggal lahir">
            @error('tgl_lahir')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea class="form-control" name="bio" placeholder="Masukkan bio">{{$user_detail->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>No HP</label>
            <input type="text" class="form-control" name="no_hp" value="{{$user_detail->no_hp}}" placeholder="Masukkan no HP">
            @error('no_hp')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/admin/user" class="btn btn-info">Back</a>
    </form>
</div>
@else   
<div>
        <form action="/admin/user_detail" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="form-group">
                <label>Tanggal Lahir</label>
                <input type="date" class="form-control" name="tgl_lahir" placeholder="Masukkan tanggal lahir">
                @error('tgl_lahir')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea class="form-control" name="bio" placeholder="Masukkan bio"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>No HP</label>
                <input type="text" class="form-control" name="no_hp" placeholder="Masukkan no HP">
                @error('no_hp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/admin/user" class="btn btn-info">Back</a>
        </form>
</div>
@endif
@endsection
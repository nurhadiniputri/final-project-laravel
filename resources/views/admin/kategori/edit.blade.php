@extends('admin.layouts.shop')
@section('judul')
    <b>Edit User Detail</b>
@endsection
@section('isi')
<div>
    <form action="/admin/kategori/{{$kategori->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama kategori</label>
            <input type="text" class="form-control" name="nama" value="{{$kategori->nama}}" placeholder="Masukkan nama kategori">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/admin/kategori/" class="btn btn-info">Back</a>
    </form>
</div>
@endsection
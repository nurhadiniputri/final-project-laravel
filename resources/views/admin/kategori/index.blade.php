@extends('admin.layouts.shop')
@section('judul')
    <b>List Kategori</b>
@endsection
@section('isi')
<a href="/admin/kategori/create" class="btn btn-primary mb-2">Tambah</a>
<div class="table-responsive">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Kategori</th>
      <th scope="col">Nama Produk</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($kategori as $key => $item)  
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{$item->nama}}</td>
      <td>
        <ul>
          @foreach ($item->tag as $value)
            <li>{{$value->produk->nama}}</li>
          @endforeach
        </ul>
      </td>
      <td>
        <form action="/admin/kategori/{{$item->id}}" method="POST">
        <a href="/admin/kategori/{{$item->id}}" class="btn btn-info">Detail</a>
        <a href="/admin/kategori/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
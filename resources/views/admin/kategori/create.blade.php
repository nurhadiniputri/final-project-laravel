@extends('admin.layouts.shop')
@section('judul')
    <b>Tambah Data Kategori</b>
@endsection
@section('isi')
<div>
        <form action="/admin/kategori" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama kategori</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan kategori">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
            <a href="/admin/kategori" class="btn btn-info">Back</a>
        </form>
</div>
@endsection
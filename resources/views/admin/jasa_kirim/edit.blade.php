@extends('admin.layouts.shop')
@section('judul')
    <b>Edit Jasa Kirim</b>
@endsection
@section('isi')
<div>
    <form action="/admin/jasa_kirim/{{$jasa_kirim->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$jasa_kirim->nama}}" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Provinsi</label>
            <input type="text" class="form-control" name="provinsi" value="{{$jasa_kirim->provinsi}}" placeholder="Masukkan provinsi">
            @error('provinsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Kota/Kabupaten</label>
            <input type="text" class="form-control" name="kota_kabupaten" value="{{$jasa_kirim->kota_kabupaten}}" placeholder="Masukkan kota/kabupaten">
            @error('kota_kabupaten')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Kecamatan</label>
            <input type="text" class="form-control" name="kecamatan" value="{{$jasa_kirim->kecamatan}}" placeholder="Masukkan kecamatan">
            @error('kecamatan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Kelurahan</label>
            <input type="text" class="form-control" name="kelurahan" value="{{$jasa_kirim->kelurahan}}" placeholder="Masukkan kelurahan">
            @error('kelurahan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Tarif</label>
            <input type="number" class="form-control" name="tarif" value="{{$jasa_kirim->tarif}}" placeholder="Masukkan tarif">
            @error('tarif')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/admin/jasa_kirim/" class="btn btn-info">Back</a>
    </form>
</div>
@endsection
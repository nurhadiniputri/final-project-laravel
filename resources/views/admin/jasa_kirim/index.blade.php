@extends('admin.layouts.shop')
@section('judul')
    <b>List Jasa Kirim</b>
@endsection
@section('isi')
<a href="/admin/jasa_kirim/create" class="btn btn-primary mb-2">Tambah</a>
<div class="table-responsive">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Provinsi</th>
      <th scope="col">Kota/Kabupaten</th>
      <th scope="col">Kecamatan</th>
      <th scope="col">Kelurahan</th>
      <th scope="col">Tarif</th>
      <th scope="col">List Checkout</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($jasa_kirim as $key => $item)  
    <tr>
      <td>{{$key+1}}</td>
      <td>{{$item->nama}}</td>
      <td>{{$item->provinsi}}</td>
      <td>{{$item->kota_kabupaten}}</td>
      <td>{{$item->kecamatan}}</td>
      <td>{{$item->kelurahan}}</td>
      <td>{{$item->tarif}}</td>
      <td>
          <ul>
            @foreach ($item->checkout as $value)
                <li>
                  {{$value->user->email}} : {{$value->tgl_pesan}}
                </li>
            @endforeach
          </ul>
      </td>
      <td>
        <form action="/admin/jasa_kirim/{{$item->id}}" method="POST">
        <a href="/admin/jasa_kirim/{{$item->id}}" class="btn btn-info">Detail</a>
        <a href="/admin/jasa_kirim/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection
@extends('admin.layouts.shop')
@section('judul')
    <b>Detail Jasa Kirim</b>
@endsection
@section('isi')
    <p>Nama : {{$jasa_kirim->nama}}</p>
    <p>Provinsi : {{$jasa_kirim->provinsi}}</p>
    <p>Kota/Kabupaten : {{$jasa_kirim->kota_kabupaten}}</p>
    <p>Kelurahan : {{$jasa_kirim->kelurahan}}</p>
    <p>Kecamatan : {{$jasa_kirim->kecamatan}}</p>
    <p>Tarif : {{$jasa_kirim->tarif}}</p>
    <a href="/admin/jasa_kirim/" class="btn btn-info">Back</a>
@endsection
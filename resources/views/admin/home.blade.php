@extends('admin.layouts.shop')
@section('judul')
    Laman Utama
@endsection
@section('isi')
@guest
    <h3>#dirumahaja dengan Beli Sayur dan Buah Online di VegeFoods</h3>
    <p style="font-size: 150%;">Ayo segera login dan pesan sekarang!</p>
@else
    <h3>#dirumahaja dengan Beli Sayur dan Buah Online di VegeFoods</h3>
    <p style="font-size: 150%;">{{Auth::user()->name}} berhasil login.</p>
@endguest
@endsection

@extends('admin.layouts.shop')
@section('judul')
    <b>Edit Produk</b>
@endsection
@section('isi')
<div>
    <form action="/admin/produk/{{$produk->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$produk->nama}}" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Stok</label>
            <input type="number" class="form-control" name="stok" value="{{$produk->stok}}" placeholder="Masukkan stok">
            @error('stok')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="deskripsi" class="form-control">{{$produk->deskripsi}}</textarea>
            @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Harga</label>
            <input type="number" class="form-control" name="harga" value="{{$produk->harga}}" placeholder="Masukkan harga">
            @error('harga')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Gambar</label>
            <input type="file" class="form-control" name="gambar">
            @error('gambar')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Kategori</label>
            <input type="text" class="form-control" name="kategori" placeholder="Pisahkan dengan koma, contoh: player,VCD player">
            @error('kategori')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="/admin/produk/" class="btn btn-info">Back</a>
    </form>
</div>
@endsection